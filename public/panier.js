const card = document.getElementById('cart-card');
const cart = document.querySelector("body > header > div.header-container > div.header-menu > nav > ul > li:nth-child(2) > a > p");
const form = document.getElementById('form-section');
productArray = JSON.parse(localStorage.getItem('products'));

const displayProduct = () =>{
    card.innerHTML = `<h2>Mon panier</h2>`;

    for(let i = 0; i < productArray.length; i++){
   
    card.innerHTML += 
    `
    <div id="cart">
    <div class= "cart-product">
    <img src="${productArray[i].image}" alt="image"/>
    <p>${productArray[i].name}</p>
    </div>
    <p class="cart-colors">${productArray[i].colors}</p>
    <p class="cart-quantity">${productArray[i].quantity}</p>
    <p class="cart-price">${productArray[i].price},00 €</p>
    </div>
    `
    };
   
    let quantityProductArray = 0;
    let priceProductArray = 0;
    for (let i = 0;i < productArray.length;i++){
        priceProductArray += productArray[i].price * productArray[i].quantity;
        quantityProductArray = parseInt(quantityProductArray) + parseInt(productArray[i].quantity);
    }

    localStorage.setItem('quantityProducts', quantityProductArray);

    card.innerHTML += `
    <div class="cart-total">
        <div class="total">
        <p>Total = ${priceProductArray},00 €</p>
        </div>
        <p id="clear-btn">Vider le panier</p>
    </div>`;

    const clearButton = document.getElementById('clear-btn');

    clearButton.addEventListener('click', ()=>{
        if(confirm('Êtes-vous certain de vouloir supprimer tous les articles du panier ?')){
            localStorage.removeItem('products');
            localStorage.removeItem('quantityProducts');
            location.href="panier.html";
        };
    });
}; // END displayProduct

// displayForm

const displayForm = () =>{
    form.innerHTML = 
    `
        <div class="form-section-title"></div>
        <form id="form">
          <h2>Livraison</h2>
          <!-- Email -->
          <div id="form-email">
            <label for="email"><i class="fas fa-envelope"></i></label>
            <div id="form-group">
              <input
                type="email"
                id="email"
                name="email"
                placeholder="Adresse e-mail"
              />
              <small id="error-email" class="non-valide"></small>
            </div>
          </div>
          <!-- User -->
          <div id="form-user-box">
            <!-- Name -->
            <div id="form-user-name">
              <label for="name"><i class="fas fa-user"></i></label>
              <div id="form-group">
                <input type="text" name="name" placeholder="Nom" id="name" />
                <small id="error-name"></small>
              </div>
            </div>
            <!-- Firtname -->
            <div id="form-user-firstname">
              <label for="firstname"><i class="fas fa-user"></i> </label>
              <div id="form-group">
                <input
                  type="text"
                  id="firstname"
                  name="firstname"
                  placeholder="Prénom"
                />
                <small id="error-firstname"></small>
              </div>
            </div>
          </div>
          <!-- Adress -->
          <div id="form-box-adress">
            <label for="adress"><i class="fas fa-building"></i></label>
            <div id="form-group">
              <input
                type="text"
                id="adress"
                name="adress"
                placeholder="Adresse"
              />
              <small id="error-adress"></small>
            </div>
          </div>
          <!-- City -->
          <div id="form-box-city">
            <label for="city"><i class="fas fa-map-marker-alt"></i></label>
            <div id="form-group">
              <input id="city" type="text" name="city" placeholder="Ville" />
              <small id="error-city"></small>
            </div>
          </div>
          <div id="form-btn">
            <button id="submit-btn" type="submit">Valider ma commande</button>
            <a href="index.html" id="form-btn-back">Retour à l'accueil</a>
          </div>
          <small id="error-submit"></small>
        </form>
    `
};


if (productArray){
    displayProduct();
    displayForm();


} else{
    document.getElementById('cart-main').style.display = "block";
    card.innerHTML = `
    <div class="empty-cart-heading">
    <span class="cart-image"><i class="fas fa-shopping-cart"></i></span>
    <h2 class="empty-card-message">Votre Panier est vide</h2>
    </div>
    <a class="keep-buy-btn" href="index.html">Continuer mes achats</a>`;
};

    // Display quantity in cart

    const quantityDisplay = () =>{

    if (!localStorage.getItem('quantityProducts')){
        cart.innerHTML = 0;
    }else{
        cart.innerText = localStorage.getItem('quantityProducts');
    }

    if(localStorage.getItem('quantityProducts') > 99){
        cart.innerText = '99+';
    }
    }

    quantityDisplay();

    // Gestion du formulaire
    validEmail = false;
        validName = false;
        validFirstName = false;
        validAdress = false;
        validCity = false;

    const formEmail = document.getElementById('email');

    // Ecouter la modification de l'email
    formEmail.addEventListener('change', function(){
        let emailRegExp = new RegExp('^[a-zA-Z0-9._%+-]+[@]{1}[a-zA-Z0-9.-]{2,}[.]{1}[a-zAA-Z]{2,6}$','g');
        let testEmail = emailRegExp.test(this.value);
        let errorEmail = document.getElementById('error-email');

        if(testEmail){
            errorEmail.style.display = 'none';
            validEmail=true;
        } else{
            errorEmail.style.display = 'block';
            errorEmail.innerHTML = "Votre e-mail doit contenir un identifiant, une ' @ ', et un ' . ' suivi du nom de domaine.";
        }
    });

// Ecouter la modification du nom
const formName = document.getElementById('name');
formName.addEventListener('change', function(){
    let nameRegExp = new RegExp('^[a-zA-ZÀ-ú-]{1,50}$','g');
    let testName = nameRegExp.test(this.value);
    errorName = document.getElementById('error-name');
    if(testName){
        errorName.style.display = 'none';
        validName=true;
    } else{
        errorName.style.display = 'block';
        errorName.innerHTML = 'Votre nom ne doit pas contenir de caractère spéciaux.';
    }
});

// Ecouter la modification du prenom
const formFirstName= document.getElementById('firstname');
formFirstName.addEventListener('change', function(){
    let firstNameRegExp = new RegExp('^[a-zA-ZÀ-ú-]{1,100}$','g');
    let testFirstName = firstNameRegExp.test(this.value);
    errorFirstName = document.getElementById('error-firstname');
    if(testFirstName){
        errorFirstName.style.display = 'none';
        validFirstname=true;
    } else{
        errorFirstName.style.display = 'block';
        errorFirstName.innerHTML = 'Votre prénom ne doit pas contenir de caractère spéciaux.';
    }
});

// Ecouter la modification de l'adresse
const formAdress = document.getElementById('adress');
formAdress.addEventListener('change', function(){
    let adressRegExp = new RegExp('^[a-zA-Z0-9À-ú- ]{1,500}$','g');
    let testAdress = adressRegExp.test(this.value);
    errorAdress = document.getElementById('error-adress');
    if(testAdress){
        errorAdress.style.display = 'none';
        validAdress=true;
    } else{
        errorAdress.style.display = 'block';
        errorAdress.innerHTML = 'Votre adresse ne doit pas contenir de caractères spéciaux.';
    }
});

// Ecouter la modification de la ville
const formCity = document.getElementById('city');
formCity.addEventListener('change', function(){
    let cityRegExp = new RegExp('^[a-zA-ZÀ-ú- ]{1,500}$','g');
    let testCity = cityRegExp.test(this.value);
    errorCity = document.getElementById('error-city');
    if(testCity){
        errorCity.style.display = 'none';
        validCity=true;
    } else{
        errorCity.style.display = 'block';
        errorCity.innerHTML = 'La ville ne doit pas contenir de caractères spéciaux.';
    }
});


// Ecoute du bouton submit
form.addEventListener('submit', function(event){
    event.preventDefault();

    

    if(validEmail && validName && validFirstname && validAdress && validCity){
        let contact = {
            firstName: document.querySelector("#firstname").value,
            lastName: document.querySelector("#name").value,
            address: document.querySelector("#adress").value,
            city: document.querySelector("#city").value,
            email: document.querySelector("#email").value
        };

        // contact = JSON.parse(localStorage.getItem('contact'));
        localStorage.setItem('contact', JSON.stringify(contact));

        // On récupère chaque id du tableau "products"
        let products = [];
        for(let i = 0; i<productArray.length;i++){
            products.push(productArray[i].id);
        };

        // Objet contact et tableau de produits à envoyer
        let toSend = {
            contact,
            products
        }

        // On fait une requête de type POST
        const fetchPost = async ()=>{
            await fetch('http://localhost:3000/api/teddies/order',{
            method: "POST",
            body: JSON.stringify(toSend),
            headers:{
                "Content-Type": "application/json",
            },
        })
        .then (res => res.json())
        // .then (data => console.log(data))
        .then (data => {
            if(data.orderId != null){
                // command passed
                localStorage.setItem('orderId', data.orderId);
                localStorage.removeItem('products');
                localStorage.removeItem('quantityProducts');
                location.href="confirmation.html";
            } else {
                // command not passed
                document.getElementById('error-submit').innerHTML = "impossible de valider la commande\nVérifiez vos données";
            }
        })
        .catch(error => document.getElementById('error-submit').innerHTML = error);
        }
        fetchPost();
} else{
    document.getElementById('error-submit').innerHTML = "Veuillez d'abord renseigner tous les champs.";
}
}
)
