const queryString = window.location.search;
const id = queryString.slice(4);
const sectionProduct = document.getElementById('product-card');
const imageProduct = document.getElementById("product-image");
const titleProduct = document.querySelector(".product-title");
const descriptionProduct = document.querySelector(".product-description");
const priceProduct = document.querySelector(".product-price");
const optionProduct = document.querySelector("#product-aside > select");
const quantityProduct = document.getElementById('product-quantity');
const buttonProduct = document.getElementById('add-cart-btn');
const cart = document.querySelector("body > header > div.header-container > div.header-menu > nav > ul > li:nth-child(2) > a > p");
let test = 'test';
let option = [];
let productData = [];


const fetchProduct =  async () => {
  await fetch("http://localhost:3000/api/teddies/" + id)
    .then(res => {
        if (res.ok){
            return res.json()
        } else{
            console.log('ERROR');
            sectionProduct.innerHTML = `<h1>Oups...</br> Une erreur s'est produite</h1>`;
        }
    })
    .then((data) => {
        productData = data;
        // console.log(productData);
        imageProduct.src = data.imageUrl;
        titleProduct.innerHTML = data.name;
        descriptionProduct.innerHTML = data.description;
        priceProduct.innerHTML = data.price / 100 + ',00 €';
        // Itérer chaque élement du tableau data.colors & les afficher en HTML
        for (let color of data.colors){
            option.push(`<option>${color}</option>`);
        }
        optionProduct.innerHTML = option;  
    })
    .catch((error) => sectionProduct.innerHTML = `<h1>Oups...</br> Une erreur s'est produite</h1>`);
    
    
}; //Fin de la fonction

fetchProduct();


// Création d'un tableau, qui récepetione les objets
const sendProduct =  () => {  
    buttonProduct.addEventListener('click', (event) =>{
        event.preventDefault();
        // Crée un tableau 'produit' pour y stocker les informations du produit

        let objectProduct = {
            id: productData._id,
            name: productData.name,
            price: productData.price / 100,
            colors: optionProduct.value,
            quantity: quantityProduct.value,
            image: productData.imageUrl
        };
        
        let productArray = JSON.parse(localStorage.getItem('products'));
        // S'il n'y a pas de produits imprime "vide" et ajoute le tableau de produits au localStorage.
        if (!productArray) {
			console.log("vide");
            localStorage.setItem('products', JSON.stringify( [objectProduct] ));
            // S'il y a déjà des produits,
        } else {
			let found = false;
			console.log(productArray);
			for (let o of productArray) {

				if ((o.id === objectProduct.id) && (o.colors === objectProduct.colors)) {
					console.log("found");
					o.quantity = parseInt(o.quantity) + parseInt(objectProduct.quantity);
					localStorage.setItem('products', JSON.stringify(productArray));
					found = true;
					break;
				}
			}
                if (!found) {
                    console.log("not found");
                    productArray.push(objectProduct);
                    localStorage.setItem('products', JSON.stringify(productArray));
                }
        }

		location.href="panier.html";
    });
};

sendProduct();

// Display quantity in cart

let quantity = localStorage.getItem('quantityProducts');

if (!localStorage.getItem('quantityProducts')){
    cart.innerHTML = 0;
}else{
    cart.innerHTML = quantity;
}

if(localStorage.getItem('quantityProducts') > 99){
    cart.innerHTML = '99+';
}else{
    console.log('ok');
}