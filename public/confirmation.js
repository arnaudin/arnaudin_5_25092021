const confirmation = document.getElementById('confirmation-section');
const getId = localStorage.getItem('orderId');
const getContact = JSON.parse(localStorage.getItem('contact'));

confirmation.innerHTML = `
    <div id="confirmation">
    <i class="far fa-check-circle"></i>
    <h2>Merci <span id="confirmation-name">${getContact.firstName}</span>,</h2>
    <h2>Votre commande a été enregistrée.</h2>
    <p class="order-id">Numéro de commande: ${getId}</p>
    <div id="confirmation-details">
    <p>Votre commande sera expédiée à</p>
    <p>${getContact.lastName.toUpperCase()} ${getContact.firstName}</p>
    <p>${getContact.address}</p>
    </div>
    <a class="return-btn">Retourner à l'accueil</a>
    </div>
`

const returnBtn = document.querySelector('.return-btn');

returnBtn.addEventListener('click',function(e){
    e.preventDefault();
    localStorage.removeItem('orderId');
    localStorage.removeItem('contact');
    localStorage.removeItem('products');
    localStorage.removeItem('quantityProducts');
    location.href="index.html";
})