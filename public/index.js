let teddiesData= [];
const main = document.getElementById('main');
const cart = document.querySelector("body > header > div.header-container > div.header-menu > nav > ul > li:nth-child(2) > a > p");

const fetchTeddies = async () =>{    
    await fetch('http://localhost:3000/api/teddies')
    .then((res) => {
        if(res.ok){
            return res.json()
        } else{
            console.log('ERROR');
            main.innerHTML = `<h1>Oups...</br> Une erreur s'est produite</h1>`;
        }
    })
    .then((data)=> (teddiesData = data))
    .catch((error) => (main.innerHTML = `<h1>Oups...</br> Une erreur s'est produite</h1>`));
    
} //Fin de fetchTeddies

const teddiesDisplay = async () =>{
    await fetchTeddies();
    
    const teddies = document.getElementById("teddies-cards");
    teddies.innerHTML = teddiesData.map((teddie) =>
    `   
        <a href="produit.html?id=${teddie._id}">
        <div class="card">
        <img src=${teddie.imageUrl} alt="la peluche ${teddie.name}" class="card-image"/>
        <div class="card-heading">
        <p>${teddie.name}</p>
        <p class="card-heading-price">${teddie.price / 100},00 €</p>
        </div>
        <p class="card-notification">Personnalise le !</p>
        </div>
        </a>
    `)
    .join("");
};

teddiesDisplay();

// Shopping Cart

let carts = document.querySelectorAll('.carts');
// Display quantity in cart

let quantity = localStorage.getItem('quantityProducts');

if (!localStorage.getItem('quantityProducts'))
{
    cart.innerHTML = 0;
}else{
    cart.innerHTML = quantity;
}

if(localStorage.getItem('quantityProducts') > 99){
    cart.innerHTML = '99+';
}else{
    console.log('ok');
}
