  <img src="https://arnaudin.gitlab.io/arnaudin_5_25092021/img/logo.png" alt="0rinoco" width="243" height="71"/>

# Orinoco

Orinoco est une entreprise e-commerce.

## Pré-requis

Voici les prérequis pour utiliser cette application:

- [Node.js](https://nodejs.org/)
- [npm](https://www.npmjs.com/)

## Installation

1. Clonez le repo: `https://gitlab.com/arnaudin/arnaudin_5_25092021.git`
2. Exécutez `npm install` à l'intérieur du projet.
3. Tapez `node server` dans la ligne de commande, et voilà !

Le serveur doit fonctionner sur `localhost` avec le port par défaut `3000`. Si le
serveur s'exécute sur un autre port pour une raison quelconque, celui-ci est imprimé sur la console au démarrage du serveur, par exemple: `Listening on port 3001`.

## Auteurs

- OpenClassrooms (back-end)
- Arnaud In (front-end)

## Remerciements

- Khalid Ouarga (mentor)
